import jwt from 'jsonwebtoken';

// .sign(<data>, <secret_key>, {})
const SECRET_KEY = process.env.SECRET || "BakeCheri API";


export const createAccessToken = ({_id, email, isAdmin}) => {
    const data = { _id, email, isAdmin };

    return jwt.sign(data, SECRET_KEY);
}


export const verify = (req, res, next) => {
    // get the token in the headers authorization
    const token = req.headers.authorization;

    if(token) {
        return jwt.verify(token.split(" ")[1], SECRET_KEY, (err, data) => {
            if(err) {
                return res.send({ auth: "Failed" });
            } else {
                next();
            }
        });
    } else {
        return res.send({
            message:"Auth Failed",
            bool: false
        });
    }
}

export const decode = (token) => {
    if(token) {
        return jwt.verify(token, SECRET_KEY, (err, data) => {
            if(err) {
                return res.send({ auth: "Failed" });
            } else {
                return jwt.decode(token, { complete: true }).payload;
            }
        });
    }
}



// const authMiddlware = async (req, res, next) => {
//     try {
//         // If token.length is lower than 500 it is our own token
//         // If token.length is higher than 500 it is google auth
//         const token = req.headers.authorization.split(" ")[1];
//         const isCustomToken = token.length < 500;

//         let decodedData;

//         if(token && isCustomToken){
//             decodedData = JWT.verify(token, 'test');

//             req.userId = decodedData?.id;
//         } else {
//             decodedData = JWT.decode(token);
//             req.userId = decodedData?.sub;
//         }

//         next();
//     } catch (error) {
//         console.log(error);
//     }
// }