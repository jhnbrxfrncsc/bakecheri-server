import express from 'express';

import { 
    allOrders,
    createOrder,
    getUserOrder
} from '../controller/ordersController.js';

const router = express.Router();

// get all products
router.get('/', allOrders);

// place order
router.post('/', createOrder);

// get user's order
router.get('/:userId', getUserOrder);

export default router;