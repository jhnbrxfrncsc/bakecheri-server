import express from 'express';

import { 
    allProducts, 
    createProduct, 
    singleProduct,
    availabilityProduct,
    updateProduct,
    deleteProduct, 
} from '../controller/productsController.js';

const router = express.Router();

// get all products
router.get('/', allProducts);

// get single product
router.get('/:prodId', singleProduct);

// create product
router.post('/', createProduct);

// change the availability of the product
router.put('/availability', availabilityProduct);

// edit product
router.put('/:prodId', updateProduct);

// delete product
router.delete('/:prodId', deleteProduct);

export default router;