import express from 'express';

import { 
    allUsers,
    registerUser,
    loginUser,
    changeUserRole,
    deleteUser,
    addCartItem,
    getUser,
    deleteCartItem
} from '../controller/usersController.js';

const router = express.Router();

// get all users
router.get('/', allUsers);

// get all users
router.get('/:userId', getUser);

// register new user
router.post('/register', registerUser);

// login user
router.post('/login', loginUser);

// change role
router.put('/change-role', changeUserRole);

// delete user
router.delete('/:userId', deleteUser);

// add cart item to the user
router.put('/add-to-cart', addCartItem);

// delete cart item 
router.put('/delete-cart-item', deleteCartItem);


export default router;