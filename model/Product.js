import mongoose from 'mongoose';

const productSchema = new mongoose.Schema({
    productName: {
        type: String,
        required: [true, "Product Name is Required!"]
    },
    productImage: {
        type: String,
        required: [true, "Product Image is Required!"]
    },
    productPrice: {
        type: Number,
        required: [true, "Product Price is Required!"]
    },
    productCategory: {
        type: [String],
        required: [true, "Product Category is Required!"]
    },
    isActive: {
        type: Boolean,
        default: false
    },
    productOrders : [
        {
            userId: {
                type: String,
                required: [true, "User ID is Required"]
            },
            orderId: {
                type: String,
                required: [true, "Order ID is Required"]
            },
            purchaseDate: {
                type: Date,
                default: new Date().toISOString().split('T')[0]
            }
        }
    ]
}, { timestamps: true });

const PostProduct = mongoose.model('products', productSchema);

export default PostProduct;