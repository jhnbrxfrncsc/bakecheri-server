import User from '../model/User.js';

// Bcrypt
import bcrypt from 'bcrypt';
import { createAccessToken, decode } from '../auth.js';

// get all users
export const allUsers = async (req, res) => {
    try {
        const users = await User.find({});
        if(users){
            res.status(200).json({
                isSuccess: true,
                message: "Successfully fetched the users.",
                data: users
            })
        } else {
            res.status(400).json({
                isSuccess: false,
                message: "We can't fetch the users from the database."
            })
        }
    } catch (error) {
        res.status(400).json({
            isSuccess: false,
            message: "There's an error. Catch error block.",
            error: error.message
        })
    }
}

// get all users
export const getUser = async (req, res) => {
    try {
        const user = await User.findById(req.params.userId);
        if(user){
            res.status(200).json({
                isSuccess: true,
            message: "Successfully fetched the user.",
                data: user
            })
        } else {
            res.status(400).json({
                isSuccess: false,
                message: "We can't fetch the products from the database."
            })
        }
    } catch (error) {
        res.status(400).json({
            isSuccess: false,
            message: "There's an error. Catch error block.",
            error: error.message
        })
    }
}

// register new user
export const registerUser = async(req, res) => {
    const { email } = req.body;
    try {
        const existingUser = await User.findOne({ email });
        if(existingUser === null){
            const salt = 12;
            const userPassword = req.body.password;
            const hashedPassword = await bcrypt.hash(userPassword, salt);
            const newData = {
                ...req.body,
                password: hashedPassword
            }
            const newUser = new User(newData);
            const result = await newUser.save();
            res.status(201).json({
                isSuccess: true,
                message: "Registration is successful. Redirecting you now to Login Page."
            });
        } else {
            res.json({ 
                isSuccess: false,
                message: `${email} already exist!`
            })
        }
    } catch (error) {
        res.status(404).json({ 
            isSuccess: false,
            message: "There's an error. Catch error block.",
            error: error.message
        })
    }
}

// login user
export const loginUser = async(req, res) => {
    const { email, password } = req.body;
    try {
        const existingUser = await User.findOne({ email });
        if(existingUser){
            const isPassValid = await bcrypt.compare(password, existingUser.password);
            if(isPassValid){
                const token = createAccessToken(existingUser);
                res.status(201).json({
                    isSuccess: true,
                    message: "Successfully Logged In.",
                    data: existingUser,
                    token
                });
            } else {
                res.json({ 
                    isSuccess: false,
                    message: `Invalid Password!`
                })
            }
        } else {
            res.json({ 
                isSuccess: false,
                message: `Invalid Email!`
            })
        }
    } catch (error) {
        res.status(404).json({ 
            isSuccess: false,
            message: "There's an error. Catch error block.",
            error: error.message
        })
    }
}

// Change User Role
export const changeUserRole = async(req, res) => {
    const { userId } = req.body;
    try {
        const user = await User.findById(userId);
        if(user !== null){
            user.isAdmin = !user.isAdmin;
            await User.findByIdAndUpdate(userId, user, { new: true })
            res.json({
                isSuccess: true,
                message: `Successfully changed the role of ${user.firstName} ${user.lastName}`
            })
        } else {
            res.json({ 
                isSuccess: false,
                message: "We can't find the user in our database.",
                error: error.message
            })
        }
    } catch (error) {
        res.status(404).json({ 
            isSuccess: false,
            message: "There's an error. Catch error block.",
            error: error.message
        })
    }
}

// Change User Role
export const deleteUser = async(req, res) => {
    try {
        const user = await User.findById(req.params.userId);
        if(user){
            await User.findByIdAndDelete(req.params.userId)
            res.json({
                isSuccess: true,
                message: `Successfully deleted ${user.email}`
            })
        } else {
            res.json({ 
                isSuccess: false,
                message: "We can't find the user in our database.",
                error: error.message
            })
        }
    } catch (error) {
        res.status(404).json({ 
            isSuccess: false,
            message: "There's an error. Catch error block.",
            error: error.message
        })
    }
}

// ADD ITEM TO CART 
export const addCartItem = async (req, res) => {
    const { userId, productName, productPrice, productImage, qty } = req.body;
    const cartItem = { productName, productPrice, productImage, qty };
    const result = await User.findById(userId);
    if(result){
        if(result.cartItems.length >= 1){
            const matchedName = result.cartItems.filter(userCart => userCart.productName === productName);

            if(matchedName.length === 0){
                result.cartItems.push(cartItem);
            } else if(matchedName.length >= 1) {
                result.cartItems.forEach(userCart => {
                    if(userCart.productName === cartItem.productName && userCart.productBrand === cartItem.productBrand) {
                        if(req.body.act === "inc"){
                            userCart.qty += 1; 
                        } else if(req.body.act === "dec"){
                            userCart.qty -= 1; 
                        } else {
                            userCart.qty += 1; 
                        }
                    }
                })
            }   
        } else if(result.cartItems.length === 0) {
            result.cartItems.push(cartItem);
        }
        await User.findByIdAndUpdate(userId, {cartItems: result.cartItems}, { new: true });
        res.json(
            {
                message: `${productName} is now added to ${result.firstName}'s Cart.`,
                isSuccess: true
            }
        )
    } else {
        res.json(
            {
                message: `We can't find user.`,
                isSuccess: false
            }
        )
    }
}

// Delete CART ITEM
export const deleteCartItem = async (req, res) => {
    const user = await User.findById(req.body.userId);
    if(user) {
        const updatedCart = user.cartItems.filter(cartItem => cartItem.productName !== req.body.productName);
        user.cartItems = updatedCart;
        await User.findByIdAndUpdate(req.body.userId, user, { new: true });
        res.json(
            {
                message: `${req.body.productName} is now removed from your cart.`, 
                isSuccess: true
            }
        );
    } else {
        res.json(
            {
                message:`Please Log in`,
                isSuccess: false
            }
        )
    }
}
