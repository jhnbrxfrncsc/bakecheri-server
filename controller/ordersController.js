import Order from '../model/Order.js';
import Product from '../model/Product.js';
import User from '../model/User.js';

// get all orders
export const allOrders = async (req, res) => {
    try {
        const orders = await Order.find({});
        if(orders){
            res.status(200).json({
                isSuccess: true,
                message: "Successfully fetched the orders.",
                data: orders
            })
        } else {
            res.status(400).json({
                isSuccess: false,
                message: "We can't fetch the orders from the database."
            })
        }
    } catch (error) {
        res.status(400).json({
            isSuccess: false,
            message: "There's an error. Catch error block.",
            error: error.message
        })
    }
}

// create order
export const createOrder = async (req, res) => {
    // const {
    //     userId,
    //     email,
    //     totalAmount,
    //     products: {
    //         productName,
    //         productImage,
    //         productPrice,
    //         subtotal,
    //         qty,
    //     }
    // } = req.body
    const newOrder = new Order(req.body);
    newOrder.save(async (err, result) => {
        if(err) {
            return res.send(err);
        } else {
            const user = await User.findById(req.body.userId);
            if(user){
                // Saving order to user
                const userOrders = {
                    orderId: result._id,
                    products: req.body.products,
                    totalAmount: req.body.totalAmount
                }
                await User.findByIdAndUpdate(req.body.userId, {cartItems: [], orders: [...user.orders, userOrders]} ,{ new: true });

                // saving to product model
                const productOrder = {
                    orderId: result._id,
                    userId: req.body.userId
                }
                req.body.products.forEach(async (product) => {
                    const prodRes = await Product.findOne({ productName: product.productName });
                    if(prodRes){
                        await Product.findByIdAndUpdate(prodRes._id, {productOrders: [...prodRes.productOrders, productOrder]} ,{ new: true });
                    } else {
                        console.log("can't find the product");
                    }
                })
                return res.json(
                    {
                        message: `new order for ${user.email}`,
                        isSuccess: true,
                        data: result
                    }
                );
            } else {
                return res.json(
                    {
                        message: `Can't find the user`,
                        isSuccess: false
                    }
                );
            }
        }
    });
}


export const getUserOrder = async(req, res) => {
    const user = await User.findById(req.params.userId);
    if(user){
        res.json({
            data: user.orders,
            isSuccess: true,
            message: `Fetching ${user.email}'s orders is successful.`
        });
    } else {
        res.json({
            message: "There's an error",
            bool: false
        })
    }
}

