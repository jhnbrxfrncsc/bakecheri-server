import Product from '../model/Product.js';

// get all products
export const allProducts = async (req, res) => {
    try {
        const products = await Product.find({});
        if(products){
            res.status(200).json({
                isSuccess: true,
                message: "Successfully fetched the products.",
                data: products
            })
        } else {
            res.json({
                isSuccess: false,
                message: "We can't fetch the products from the database."
            })
        }
    } catch (error) {
        res.status(400).json({
            isSuccess: false,
            message: "There's an error. Catch error block.",
            error: error.message
        })
    }
}

// get single product
export const singleProduct = async (req, res) => {
    try {
        const product = await Product.findById(req.params.prodId);
        if(product){
            res.status(200).json({
                isSuccess: true,
                message: "Successfully fetch the single product from the database.",
                data: product
            });
        } else {
            res.json({ 
                isSuccess: false,
                message: "We can't find the product."
            })
        }
    } catch (error) {
        res.status(404).json({ 
            isSuccess: false,
            message: "There's an error. Catch error block.",
            error: error.message
        })
    }
}

// create new product
export const createProduct = async(req, res) => {
    const { productName } = req.body;
    try {
        const existingProduct = await Product.findOne({ productName });
        if(existingProduct === null){
            const newProduct = new Product(req.body);
            const result = await newProduct.save();
            res.status(201).json({
                isSuccess: true,
                message: "Successfully created a new product.",
                data: result
            });
        } else {
            res.json({ 
                isSuccess: false,
                message: `${productName} already exist!`
            })
        }
    } catch (error) {
        res.status(404).json({ 
            isSuccess: false,
            message: "There's an error. Catch error block.",
            error: error.message
        })
    }
}

export const availabilityProduct = async(req, res) => {
    try {
        const { prodId } = req.body;
        const product = await Product.findById(prodId);
        if(product !== null){
            product.isActive = !product.isActive;
            const updatedProduct = await Product.findByIdAndUpdate(prodId, product, { new: true });
            res.status(200).json({
                isSuccess: true,
                message: `Successfully updated the ${updatedProduct.productName} product.`,
                data: updatedProduct
            });
        } else {
            res.json({ 
                isSuccess: false,
                message: "We can't find the product in our database."
            })
        }
    } catch (error) {
        res.status(404).json({ 
            isSuccess: false,
            message: "There's an error. Catch error block.",
            error: error.message
        })
    }
}


export const updateProduct = async(req, res) => {
    try {
        const product = await Product.findById(req.params.prodId);
        console.log(req.body);
        
        if(product){
            const updatedProduct = await Product.findByIdAndUpdate(req.params.prodId, req.body, { new: true });
            res.status(200).json({
                isSuccess: true,
                message: `Successfully updated the "${updatedProduct.productName}" product.`,
                data: updatedProduct
            })
            
        } else {
            res.json({ 
                isSuccess: false,
                message: "We can't find the product (_id) to be updated."
            })
        }
    } catch (error) {
        res.status(404).json({ 
            isSuccess: false,
            message: "There's an error. Catch error block.",
            error: error.message
        })
    }
}


export const deleteProduct = async(req, res) => {
    await Product.findByIdAndRemove(req.params.prodId);
    res.json({ 
        isSuccess: true,
        message: "Post deleted successfully." 
    });
}
